const {User} = require('../models/userModel');

const {
  DataError,
  InvalidRequestError,
} = require('../utils/errors');

const getUserInfo = async (_id) => {
  const user = await User.find({_id}, {__v: 0});
  if (!user) {
    throw new InvalidRequestError('User is absent');
  }
  return user;
};

const deleteUserInfo = async (_id) => {
  await User.findOneAndRemove({_id});
};

module.exports = {
  getUserInfo,
  deleteUserInfo,
};
