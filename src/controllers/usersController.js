const express = require('express');
const router = new express.Router();

const {
  getUserInfo,
  deleteUserInfo,
} = require('../services/userService');

const {tryCatchWrapper} = require('../utils/apiUtils');

const {passwordValidator} = require('../middlewares/validationMidlleware');

router.get('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const user = await getUserInfo(userId);
  res.json({user});
}));

router.delete('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  await deleteUserInfo(userId);
  res.json({message: 'Profile deleted successfully'});
}));

module.exports = {
  usersRouter: router,
};
