// const {PermissionError} = require('../utils/errors');

const tryCatchWrapper = (callback) => {
  return (req, res, next) =>
    callback(req, res, next)
        .catch(next);
};

// const SECRET = 'kln4$hjbk(l)nLKJB';

// const isUserDriver = (role, trueRole = 'DRIVER') => role === trueRole;
// {
//   if (role !== trueRole) {
//     throw new PermissionError('You should be driver to do this.');
//   }
// };

// const isUserShipper = (role, trueRole = 'SHIPPER') => role === trueRole;
// {
//   if (role !== trueRole) {
//     throw new PermissionError('You should be shipper to do this.');
//   }
// };

// const checkRole = hasUserAppropriateRole => {
//   if (!hasUserAppropriateRole) {
//     throw new PermissionError("You don't have permissions to do this.");
//   }
// }

module.exports = {
  tryCatchWrapper,
  // SECRET,
  // isUserDriver,
  // isUserShipper,
  // checkRole,
};