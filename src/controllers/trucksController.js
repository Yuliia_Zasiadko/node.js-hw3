const express = require('express');
const router = new express.Router();

const {
  getAllTrucksByDriverId,
  addTruckByDriverId,
  getTruckById,
  assignTruckToDriver,
  doesTruckHaveStatusIS,
  reassignTruckToDriver,
  isTruckNotAssigned,
  changeTruckInfo,
  deleteTruck,
} = require('../services/trucksService');

const {tryCatchWrapper} = require('../utils/apiUtils');

const {
  StatusError,
  InvalidRequestError,
} = require('../utils/errors');

const {truckValidator} = require('../middlewares/validationMidlleware');

router.get('/', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const trucks = await getAllTrucksByDriverId(userId);
  if (!trucks) {
    throw new InvalidRequestError(`Invalid request`);
  }
  res.json({trucks: trucks});
}));

router.post('/', truckValidator, tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const {type} = req.body;
  await addTruckByDriverId(type, userId);
  res.json({message: 'Truck created successfully'});
}));

router.get('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const truck = await getTruckById(truckId, userId);
  res.json({truck});
}));

router.post('/:id/assign', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  await assignTruckToDriver(truckId, userId);
  res.json({message: 'Truck assigned successfully'});
}));

router.post('/:id/reassign', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  if (!(await doesTruckHaveStatusIS(truckId))) {
    throw new StatusError (`Truck ${truckId} doesn't have status 'IS'`);
  }
  await reassignTruckToDriver(truckId, userId);
  res.json({message: 'Truck reassigned successfully'});
}));

router.put('/:id', truckValidator, tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  const {type} = req.body;
  await isTruckNotAssigned(truckId)
  
  await changeTruckInfo(truckId, userId, type);
  res.json({message: 'Truck details changed successfully'});
}));

router.delete('/:id', tryCatchWrapper(async (req, res) => {
  const {userId} = req.user;
  const truckId = req.params.id;
  await isTruckNotAssigned(truckId)

  await deleteTruck(truckId, userId);
  res.json({message: 'Truck deleted successfully'});
}));

module.exports = {
  trucksRouther: router,
};
