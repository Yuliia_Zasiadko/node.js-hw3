const jwt = require('jsonwebtoken');
const {AuthError} = require('../utils/errors');
// const {SECRET} = require('../utils/apiUtils');
require('dotenv').config({path: './.env'});
const SECRET = process.env.SECRET;

const authMiddleware = (req, res, next) => {
  console.log('--authorization--');
  const {authorization} = req.headers;

  if (!authorization) {
    throw new AuthError('Please, provide "authorization" header');
  }

  const [, jwt_token] = authorization.split(' ');

  if (!jwt_token) {
    throw new AuthError('Please, include token to request');
  }

  try {
    const tokenPayload = jwt.verify(jwt_token, SECRET);
    req.user = {
      userId: tokenPayload._id,
      email: tokenPayload.email,
      role: tokenPayload.role,
    };
    console.log("🚀 ~ file: authMiddleware.js ~ line 24 ~ authMiddleware ~ req.user ", req.user );
    next();
  } catch (err) {
    throw new AuthError('Wrong token');
  }
};

module.exports = {
  authMiddleware,
}