/**
 * Represents a UberTrucksError.
 */
class UberTrucksError extends Error {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 500;
  }
}

/**
 * Represents a InvalidRequestError.
 */
class InvalidRequestError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a AuthorizationError.
 */
class AuthError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a PermissionError.
 */
class PermissionError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a DataError.
 */
class DataError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a StatusError.
 */
class StatusError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

/**
 * Represents a ValidationError.
 */
class ValidationError extends UberTrucksError {
  /**
  * @param {error.message} message The error.message.
  */
  constructor(message) {
    super(message);
    this.status = 400;
  }
}

module.exports = {
  UberTrucksError,
  InvalidRequestError,
  AuthError,
  DataError,
  PermissionError,
  StatusError,
  ValidationError,
};
