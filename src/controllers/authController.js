const express = require('express');
const router = new express.Router();

const {
  registration,
  login,
  resetPassword,
} = require('../services/authService');

const {userValidator} = require('../middlewares/validationMidlleware');

const {tryCatchWrapper} = require('../utils/apiUtils');

router.post('/register', userValidator, tryCatchWrapper(async (req, res) => {
  console.log('--register--');
  const {email, password, role} = req.body;
  await registration({email, password, role});
  res.json({message: 'Profile created successfully'});
}));

router.post('/login', tryCatchWrapper(async (req, res) => {
  const {email, password} = req.body;
  const jwt_token = await login({email, password});
  res.json({jwt_token});
}));

router.post('/forgot_password', tryCatchWrapper(async (req, res) => {
  const {email} = req.body;
  await resetPassword({email});
  res.json({message: 'New password sent to your email address'});
}));

module.exports = {
  authRouter: router,
};
