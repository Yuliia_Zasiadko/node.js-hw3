const {PermissionError} = require('../utils/errors');

const isUserShipper = (req, res, next) => {
  const {role} = req.user;
  console.log("🚀 ~ file: isUserShipper.js ~ line 5 ~ isUserShipper ~ role", role);
  
  if (role !== 'SHIPPER') {
    throw new PermissionError('You should be a shipper to do this.');
  }
  console.log("🚀 ~ file: isUserShipper.js ~ line 10 ~ isUserShipper", true);
  next();
  return true;
};

const isUserDriver = (req, res, next) => {
  const {role} = req.user;
  console.log("🚀 ~ file: isUserDriver.js ~ line 17 ~ isUserDriver ~ role", role);
  
  if (role !== 'DRIVER') {
    throw new PermissionError('You should be a driver to do this.');
  }
  console.log("🚀 ~ file: isUserDriver.js ~ line 22 ~ isUserDriver", true);
  next();
  return true;
};

module.exports = {
  isUserShipper,
  isUserDriver,
};
