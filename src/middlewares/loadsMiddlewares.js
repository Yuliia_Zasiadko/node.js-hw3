const {
  createLoad,
  changeLoadStatusFromOldToNew,
  getLoadById,
  assignDriverToLoad,
  setTruckIdToLoad,
  setLoadState,
  addLogToLoad,
} = require('../services/loadsService');

const {
  getAppropriateTruckTypes,
  findTruckWithGivenTruckTypes,
  setStatusToTruck,
} = require('../services/trucksService');

const {tryCatchWrapper} = require('../utils/apiUtils');

const addLoadtoShipper = tryCatchWrapper(async (req, res, next) => {
  console.log('--addLoadstoShipper');
  // console.log(req.user, req.body);
  const {userId} = req.user;
  const loadInfo = req.body;
  const load = await createLoad(userId, loadInfo);
  console.log("🚀 ~ file: loadsMiddlewares.js ~ line 11 ~ addLoadtoShipper ~ load", load)
  // req.loadId = load._id;
  console.log('++addLoadstoShipper');
  // const loadId = load._id;
  // res.redirect(307, `/api/loads/${loadId}/post`);
  // next();
  res.json({message: 'Load created successfully'});
});

const postLoad = tryCatchWrapper(async (req, res, next) => {
  console.log('--postLoad');
  console.log(req.params);
  // const loadId = req.loadId || 
  const loadId = req.params.id;
  console.log(loadId);
  // req.loadId = loadId;
  // const resultMessage = 
  await changeLoadStatusFromOldToNew(loadId, 'NEW', 'POSTED');
  // console.log(resultMessage);
  // await addLogToLoad(loadId, resultMessage);
  console.log("++postLoad")
  // console.log(req.loadId);
  // res.json({message: 'postLoad is successful'});
  next();
});

const findTruckForLoad = tryCatchWrapper(async (req, res, next) => {
  console.log('--findTruckForLoad');
  // const loadId = req.loadId;
  const loadId = req.params.id;
  const load = await getLoadById(loadId);
  const loadInfo = {
    dimensions: load.dimensions,
    payload: load.payload,
  };
  console.log("🚀 ~ file: loadsMiddlewares.js ~ line 57 ~ findTruckForLoad ~ loadInfo", loadInfo)
  const appropriateTruckTypes = await getAppropriateTruckTypes(loadInfo);
  console.log("🚀 ~ file: loadsMiddlewares.js ~ line 59 ~ findTruckForLoad ~ appropriateTruckTypes", appropriateTruckTypes)
  const truck = await findTruckWithGivenTruckTypes(appropriateTruckTypes);
  console.log("🚀 ~ file: loadsMiddlewares.js ~ line 61 ~ findTruckForLoad ~ truck", truck)
  
  let driver_found = false;
  if (truck) {
    driver_found = true;

    await setStatusToTruck(truck._id, 'OL');

    await assignDriverToLoad(loadId, truck.assigned_to);

    await changeLoadStatusFromOldToNew(loadId, 'POSTED', 'ASSIGNED');

    await setTruckIdToLoad(loadId, truck._id);

    await setLoadState(loadId, 'En route to Pick Up');
  } else {
    await addLogToLoad(loadId, 'Appropriate truck was not found');
    
    await changeLoadStatusFromOldToNew(loadId, 'POSTED', 'NEW');
  }

  console.log('++findTruckForLoad');
  res.json({message: 'findTruckForLoad is successful', driver_found});
});

const doesLoadHaveStatusNew = tryCatchWrapper(async (req, res, next) => {
  const loadId = req.params.id;
  const load = await getLoadById(loadId);
  if (load.status !== 'NEW') {
    throw new DataError (`${loadId} load doesn't have status 'NEW`);
  }
});

module.exports ={
  addLoadtoShipper,
  postLoad,
  findTruckForLoad,
  doesLoadHaveStatusNew,
};
