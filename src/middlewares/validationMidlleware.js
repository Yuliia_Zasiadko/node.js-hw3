const Joi = require('joi');

const {tryCatchWrapper} = require('../utils/apiUtils');

const {ValidationError} = require('../utils/errors');

const userValidator = tryCatchWrapper(async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string()
      .email()
      .required(),
    password: Joi.string()
      .min(1)
      .max(40)
      .required(),
    role: Joi.string()
      .valid('SHIPPER', 'DRIVER')
      .required()
  });

  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    throw new ValidationError(`${err}`);
  }
});

const passwordValidator = tryCatchWrapper(async (req, res, next) => {
  const schema = Joi.object({
    password: Joi.string()
      .min(1)
      .max(40)
      .required(),
  });
  
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    throw new ValidationError(`${err}`);
  }
});

const truckValidator = tryCatchWrapper(async (req, res, next) => {
  const schema = Joi.object({
    type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .required(),
  });
  
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    // next(err);
    throw new ValidationError(`${err}`);
  }
});

const loadValidator = tryCatchWrapper(async (req, res, next) => {
  const schema = Joi.object({
    name: Joi.string()
      .required(),
    payload: Joi.number()
      .less(10000)
      .required(),
    pickup_address: Joi.string()
      .required(),
    delivery_address: Joi.string()
      .required(),
    dimensions: {
      width: Joi.number()
        .less(500)
        .required(),
      length: Joi.number()
        .less(1200)
        .required(),
      height: Joi.number()
        .less(300)
        .required(),
    },
    type: Joi.string()
      .valid('NEW'),
  });
  
  try {
    await schema.validateAsync(req.body);
    next();
  } catch (err) {
    throw new ValidationError(`${err}`);
  }
});

module.exports = {
  userValidator,
  passwordValidator,
  truckValidator,
  loadValidator,
};
