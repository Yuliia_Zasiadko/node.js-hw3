const TRUCK_TYPES = [{
  type: 'SPRINTER',
  dimensions: {
    length: 300,
    width: 250,
    height: 170,
  },
  payload: 1700,
}, {
  type: 'SMALL STRAIGHT',
  dimensions: {
    length: 500,
    width: 250,
    height: 170,
  },
  payload: 2500,
}, {
  type: 'LARGE STRAIGHT',
  dimensions: {
    length: 700,
    width: 350,
    height: 200,
  },
  payload: 4000,
}];

module.exports = {
  TRUCK_TYPES,
};
