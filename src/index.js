require('dotenv').config();
const PORT = process.env.PORT;
const DB_PATH = process.env.DB_PATH;

const express = require('express');
const app = express();

const morgan = require('morgan');
const mongoose = require('mongoose');

const {authMiddleware} = require('./middlewares/authMiddleware');
const {isUserDriver} = require('./middlewares/roleMiddleware');

const {authRouter} = require('./controllers/authController');
const {usersRouther} = require('./controllers/usersController');
const {trucksRouther} = require('./controllers/trucksController');
const {loadsRouther} = require('./controllers/loadsController');

const {TRUCK_TYPES} = require('./utils/truckTypeInfoForDB');
const {addTruckTypeToDB} = require('./services/truckTypeService');

const {UberTrucksError} = require('./utils/errors');

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);

app.use(authMiddleware);
// app.use('/api/users/me', usersRouther);
app.use('/api/trucks', isUserDriver, trucksRouther);
app.use('/api/loads', loadsRouther);

app.use((req, res, next) => {
  res.status(404).json({
    message: `The requested URL ${req.url} was not found`,
  });
});

app.use((err, req, res, next) => {
  if (err instanceof UberTrucksError) {
    return res.status(err.status).json({
      message: err.message,
    });
  }
});

(async () => {
  try {
    await mongoose.connect(DB_PATH, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    });

    TRUCK_TYPES.forEach(addTruckTypeToDB);

    app.listen(PORT);
  } catch (err) {
      console.error(`Error with server startup: ${err.message}`)
  }
})();
