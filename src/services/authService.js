const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

require('dotenv').config({path: './.env'});
const SECRET = process.env.SECRET;

const {User} = require('../models/userModel');

const {
  AuthError,
  DataError,
} = require('../utils/errors');

const registration = async ({email, password, role}) => {
  const user = new User({
    email,
    role,
    password: await bcrypt.hash(password, 10),
  });
  try {
    await user.save();
  } catch (err) {
    throw new DataError('You have been already registered');
  }
};

const login = async ({email, password}) => {
  const user = await User.findOne({email});

  if (!user) {
    throw new AuthError('Invalid email or password');
  }

  if (!(await bcrypt.compare(password, user.password))) {
    throw new AuthError('Invalid email or password');
  }

  return jwt.sign({
    _id: user._id,
    email: user.email,
    role: user.role,
  }, SECRET);
};

const resetPassword = async ({email}) => {
  const newPassword = Math.random().toString(36).slice(-10)
};

module.exports = {
  registration,
  login,
  resetPassword,
};
